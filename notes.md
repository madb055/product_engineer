### What does Plaid do? ###

* Plaid builds a data transfer network that powers Fintech and digital finance products. Offers normal apps the ability to provide banking services without having to build their own infrastructure.

    - Transactions: Retrieve up to 24 months of transaction data from accounts and stay up-to-date with webhooks.
    - Auth:Retrieve and verify bank account information.
    - Balance: Retrieve real-time balance information.
    - Identity: Verify users' identities and reduce fraud with the Identity product. 
    - Assets: Access users' financial information for loan underwriting.
    - Investments: View holdings and transactions from investment accounts.
    - Liabilities: Access data for student loans, mortgages, and credit cards.
    - Payment Initiation(Europe): Initiate payments within your app.

### Which of these can Staircase leverage? ###
- All?



### How and Where? ###

#### Connector ####

Connector is a RESTful, scalable API that serves as a foundation layer for integrations between Staircase and Staircase's many data partners. Its fast-to-market architecture with prebuilt services and behavioral patterns conceals the complexity inherent with integrations and enables the rapid development and delivery of new data partner connections.

##### Review Integration Guide
The Lender reviews the custom partner's integration guide to determine  :
* Request and response schemas
* Requirements for authorization and authentication

This information is needed to create connectiosn with Plaid and to add the custom partner's language to Staircase language.

##### Create Connections

Create a custom connection between Staircase and Plaid with three connector API calls:
* CreateNewPartner adds the partner name and the lender's partner credentials to Staircase
* CreateNewFlow: defines an **asynchronous** integration between the partner and Staircase. The flow definition includes the version of the flow specification("2"), aname for the flow, credentials to call the partner's API and the partner services that need to be called.
* CreateNewLookup defines a **synchronous** integration between the partner and Staircase. Similar to a flow, the lookup definition includes the version of the lookup specification("2"), a name for the lookup, credentials to call the partner's API and the partner services that need to be called.


#### Translator ####

Translator performs language translations between Staircase and Staircase data partners using mortgage industry standard languages like MISMO, as well as exclusive languages used by a single partner. It gives customers zero burden optionality while also concealing the complexity associated with managing integrations that require the use of multiple partner languages.

The lender maps the objects in the partner language(schema) with the corresponding objects in the Staircase language(schema), then creates the partner's language in Staircase with one Translator API call.
* CreateNewLanguage creates, in Staircase, a new mapping between the partner's language and Staircase language.


##### Launch to marketplace
The Lender creates a data bundle or deployable artifact, from the custom partner connectiosn and laguages; and launches the product on Marketplace. once on marketplace, the data bundle is available in the lender's environment to all users with access to the environment.

* CreateNewLanguage creates, in Staircase, a new mapping between the partner's language and Staircase language.

#### Extensions ####
Beyond onboarding, Plaid enables SoLo to perform due diligence on every borrower. Along with other metrics, they leverage a borrower’s banking transactions and cash flow over time to assess creditworthiness and predict their ability to repay loans on time. The analysis yields a borrower’s SoLo Score, which gives potential lenders an additional datapoint to consider when making a decision.