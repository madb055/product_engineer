#### Questions & Comments ####
## Vision ###
* Staircase wants to be the "AWS of mortgage"? What does that actually mean to you?
* Since it is early stages, perhaps Staircase should try as much as is reasonably possible to avoid technical debt by designing for longevity? It shouldn't be a matter of getting things to "just work", but trying to thoroughly think through the solutions given the constraints. Otherwise, you will build yet another unreliable mortgage solution.
* Perhaps consider that a long experience in the mortgage industry gives you the most insight of what **NOT** to do. Not the other way round.
* Think from first pricinples rather than thinking by analogy
* 80% availability mentioned in Plaid examples is appauling! And you can't wait for customers to make this request because they wouldn't know that they need it. Why are these metrics so dismal?


## Technical decisions ##
* Why does Translator use csv? Why not just stick to json? (perhaps offer an online tool to convert csv to json)
* API versioning?
* Are there SDKs or client libraries?
* How do you currently handle failures in you downstream providers? Can that get better?
* Why not implement multi-region architecture? What's the blocker?


## Documentation ##
* What is the documentation ownership model like? Is each team responsible for it's documentation or is there a sole person/team in charge of all the documentation?
* May I propose a little more attention to detail i.e. English grammar, completeness, examples?
* Technical blog writeups are normally a good way to show case what can be done with the APIs


## Future Pipeline
* Are there any plans for ML/AI applications somewhere along the Staircase pipeline? I'm asking this because the recruiter gave me the impression that that was one of my desired skills.

