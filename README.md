# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Skills Assessment ###

* Read [Staircase documentation](http://api.staircase.co/) 
* Read [Plaid Integration Guide](https://plaid.com/docs/quickstart/) 
* Write a series of user stories to onboard plaid as a partner, using Staircase Connector and Translator products.
* Each user story:
    - is done in one iteration (one week) 
    - has customer value
    - is demoable 
    - follows the shortest path to the complete solution


The demo will consist of story walkthroughs and verbal justification for why each story satisfies the above criteria. This will be a 45 min video interview.


### Integrating Connector and Translator
#### Create Connections

Create a custom connection between Staircase and Plaid with three connector API calls:

* CreateNewPartner adds the partner name and the lender's partner credentials to Staircase
* CreateNewFlow: defines an **asynchronous** integration between the partner and Staircase. The flow definition includes the version of the flow specification("2"), aname for the flow, credentials to call the partner's API and the partner services that need to be called.
* CreateNewLookup defines a **synchronous** integration between the partner and Staircase. Similar to a flow, the lookup definition includes the version of the lookup specification("2"), a name for the lookup, credentials to call the partner's API and the partner services that need to be called.


#### Translator ####

Translator performs language translations between Staircase and Staircase data partners using mortgage industry standard languages like MISMO, as well as exclusive languages used by a single partner. It gives customers zero burden optionality while also concealing the complexity associated with managing integrations that require the use of multiple partner languages.

The lender maps the objects in the partner language(schema) with the corresponding objects in the Staircase language(schema), then creates the partner's language in Staircase with one Translator API call.

* CreateNewLanguage creates, in Staircase, a new mapping between the partner's language and Staircase language.


## User Stories ##

### Design
**As a** Staircase engineer, **I want** to be understand the design considerations regarding the Plaid integration **so that** I can know how the system is intended to work.

#### Acceptance Criteria
* Must be peer-reviewed
* Request and response schemas
* Design Document
* Data model
* Security threat model and mitigations
* Scalability? Availability?

#### Resources
* Read [Plaid Integration Guide](https://plaid.com/docs/quickstart/)

---

### Plaid Versioning
**As a** lender, **I want** to be able to choose which version of the Plaid API to query **so that** I'm certain what features/data formats will be returned

#### Acceptance Criteria
* If a lender chooses a particular API version, he should get responses back that match that version

#### Resources
* [Plaid Versioning](https://plaid.com/docs/api/versioning/)

---

### Token Exchange
**As a** Staircase engineer, **I want** to obtain a Plaid access_token on behalf of the lender **so that** I can make authorized calls on behalf of the lender.

#### Acceptance Criteria
* If Staircase engineer uses the received access_token, he/she should be able to authorize further Plaid calls on behalf of the lender.

#### Resources
* [Plaid Token](https://plaid.com/docs/api/tokens/)

---

### Auth

**As a** lender, **I want** to retrieve and verify bank account information **so that** I can authorize future calls.

#### Acceptance Criteria
* If lender invokes this API call in Sandbox, he should receive the appropiate Plaid-like mocked data
* If lender invokes this API call in any environment, he should receive response from Plaid

#### Resources
* [Plaid Auth](https://plaid.com/docs/auth/)

---

### Identity

**As a** lender, **I want** to verify a user's identiy using Plaid **so that** I can reduce fraud

#### Acceptance Criteria
* If lender invokes this API call in Sandbox, he should receive the appropiate Plaid-like mocked data
* If lender invokes this API call in any environment, he should receive response from Plaid


#### Constraints
Depends on **Token Exchange**

#### Resources

* [Plaid Identity](https://plaid.com/docs/identity/)

---

### Transactions

**As a** lender, **I want** to retrieve up to 24 months of transaction data from accounts and stay up-to-date with webhooks **so that** I can facilitate my underwriting decision.

Should we consider caching these?

#### Constraints
Depends on **Token Exchange**

#### Acceptance Criteria
* If lender invokes this API call in Sandbox, he should receive the appropiate Plaid-like mocked data
* If lender invokes this API call in any environment, he should receive response from Plaid

#### Resources

* [Plaid Transactions](https://plaid.com/docs/transactions/)

---

### Balance

**As a** lender, **I want** to retrieve real-time balance information on behalf of the borrower **so that** I can facilitate my underwriting decision

#### Constraints
Depends on **Token Exchange**

#### Acceptance Criteria
* If lender invokes this API call in Sandbox, he should receive the appropiate Plaid-like mocked data
* If lender invokes this API call in any environment, he should receive response from Plaid

#### Resources

* [Plaid Balance](https://plaid.com/docs/balance/)

---

### Assets

**As a** lender, **I want** to access users' financial information **so that** I can facilitate loan underwriting.


#### Acceptance Criteria
* If lender invokes this API call in Sandbox, he should receive the appropiate Plaid-like mocked data
* If lender invokes this API call in any environment, he should receive response from Plaid


#### Resources
* [Plaid Assets](https://plaid.com/docs/assets/)

---

### Investments

View holdings and transactions from investment accounts.

#### Acceptance Criteria
* If lender invokes this API call in Sandbox, he should receive the appropiate Plaid-like mocked data
* If lender invokes this API call in any environment, he should receive response from Plaid

#### Resources
* [Plaid Investments](https://plaid.com/docs/investments/)

---

### Liabilities

**As a** lender, **I want** to access data for student loans, mortgages, and credit cards **so that** I can facility loan underwriting.

#### Acceptance Criteria
* If lender invokes this API call in Sandbox, he should receive the appropiate Plaid-like mocked data
* If lender invokes this API call in any environment, he should receive response from Plaid

#### Resources
* [Plaid Liabilities](https://plaid.com/docs/liabilitiess/)

---
### Telemetry

**As a** staircase engineer, **I want** to have the metrics around our downstream service calls to Plaid **so that** I can know what calls are failing and have high latencies and plan for improvements where possible.

These should be on a service dashboard.

#### Acceptance Criteria
* Peer-reviewed

#### Resources

* [AWS Well-Architected Framework](https://docs.aws.amazon.com/wellarchitected/latest/operational-excellence-pillar/design-telemetry.html)

---

### Operational Readiness

**As a** staircase engineer, **I want** to have operational tools **so that** I can deploy, run and monitor this service effectively.

Examples of tools: operational metric alarms, log downloaders and greppers, alarmm playbook, oncall logs, deployment pipelines etc

#### Acceptance Criteria
* Peer-reviewed

---

### Documentation
**As a** lender, **I want** to have documentation about Staircase & Plaid integration **so that** I know how to access the Plaid data.

This story should validate all the documentation above.

#### Acceptance Criteria
* Peer-reviewed

